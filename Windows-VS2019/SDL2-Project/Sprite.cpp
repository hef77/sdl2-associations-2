#include "Sprite.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"

#include<stdexcept>

Sprite::Sprite() {

	//Sprite speed
	speed = 0.0f;

	targetRectangle.x = 0;
	targetRectangle.y = 0;
	targetRectangle.h = 0;
	targetRectangle.w = 0;

	//Position and velocity
	position = nullptr;
	velocity = nullptr;

	//Set texture pointer
	texture = nullptr;

	//Animation array size
	animations = nullptr;
	maxAnimations = 0;
}

Sprite::~Sprite() {

	//Deallocate memory. Set pointers back to null
	delete position;
	position = nullptr;

	delete velocity;
	velocity = nullptr;

	//Clean up animations - free memory
	for (int i = 0; i < maxAnimations; i++) {
		delete animations[i];
		animations[i] = nullptr;
	}

	delete [] animations;
	animations = nullptr;

	SDL_DestroyTexture(texture);
	texture = nullptr;
}

void Sprite::init(SDL_Renderer* renderer, string texturePath, int maxAnimations, Vector2f* initPos) {

	//Set up max animations 
	this->maxAnimations = maxAnimations;

	//Allocate position and velocity - set values
	position = new Vector2f(initPos);
	velocity = new Vector2f();
	velocity->zero();

	//Create player texture from file, optimised for renderer
	texture = createTextureFromFile(texturePath.c_str(), renderer);

	if (texture == nullptr) 
		throw std::runtime_error("File not found!");

		//Create aray of pointers to animations. Child class will have to init these
		animations = new Animation * [maxAnimations];

		//Allocate memory for the animation structures
		for (int i = 0; i < maxAnimations; i++) {
			animations[i] = new Animation();
		}
	
}

void Sprite::draw(SDL_Renderer* renderer) {

	//Get current animation based on the state
	Animation* current = this->animations[getCurrentAnimationState()];

	SDL_RenderCopy(renderer, texture, current->getCurrentFrame(), &targetRectangle);
}

void Sprite::update(float timeDeltaInSeconds) {

	//Calculate distance travelle since the last update
	Vector2f movement(velocity);
	movement.scale(timeDeltaInSeconds);

	//Update player position
	position->add(&movement);

	//Move sprite to nearest pixel location
	targetRectangle.x = round(position->getX());
	targetRectangle.y = round(position->getY());

	//Get the current animation
	Animation* current = animations[getCurrentAnimationState()];

	//Let animation update itself
	current->update(timeDeltaInSeconds);
}

Vector2f* Sprite::getPosition() {
	return position;
}