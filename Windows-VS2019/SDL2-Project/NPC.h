#ifndef NPC_H_
#define NPC_H_

#include "SDL2Common.h"
#include "Sprite.h"

//Forward declarations. Improve compile time
class Vector2f;
class Animation;
class Game;

class NPC : public Sprite {
private:

	//Animation state
	int state;

	//Sprite information
	static const int SPRITE_HEIGHT = 64;
	static const int SPRITE_WIDTH = 32;

	float maxRange;
	float timeToTarget;

	Game* game;

public:

	NPC();
	~NPC();

	//Player animation state
	enum NPCState{LEFT =0, RIGHT, UP, DOWN, IDLE};

	//Overriding virtual parents
	void init(SDL_Renderer* renderer);
	//void update(float timeDeltaInSeconds);

	//Update "things" ai related
	void ai();
	void update(float dt);

	//Overriding pure virtual - make concrete
	int getCurrentAnimationState();

	void setGame(Game* game);
};

#endif