#include "NPC.h"
#include "Player.h"
#include "Animation.h"
#include "Vector2f.h"
#include "TextureUtils.h"
#include "Game.h"

NPC::NPC() : Sprite() {
	state = IDLE;
	speed = 30.0f;
	maxRange = 5.0f;
	timeToTarget = 2.0f;

	game = nullptr;

	targetRectangle.w = SPRITE_WIDTH;
	targetRectangle.h = SPRITE_HEIGHT;
}

NPC::~NPC() {

}

void NPC::init(SDL_Renderer* renderer) {

	//Path string
	string path("assets/images/undeadking.png");

	//position
	Vector2f position(300.0f, 300.0f);

	//Call sprite constructor
	Sprite::init(renderer, path, 5, &position);

	//Setup animnation structure
	animations[LEFT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 1);
	animations[RIGHT]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 2);
	animations[UP]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 3);
	animations[DOWN]->init(3, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);
	animations[IDLE]->init(1, SPRITE_WIDTH, SPRITE_HEIGHT, -1, 0);

	for (int i = 0; i < maxAnimations; i++) {
		animations[i]->setMaxFrameTime(0.2f);
	}
}

int NPC::getCurrentAnimationState() {
	return state;
}

void NPC::ai() {
	//get player	
	Player* player = game->getPlayer();

	//Get distance from player
	Vector2f vectorToPlayer(player->getPosition());

	//Subtract our position to get a vector to the player
	vectorToPlayer.sub(position);

	float distance = vectorToPlayer.length();

	//if player in range stop and fire
	if (distance < maxRange) {
		//fire
		//fire(vectorToPlayer);

	}
	else {
		//Could do with an assign in vector
		velocity->setX(vectorToPlayer.getX());
		velocity->setY(vectorToPlayer.getY());
		printf("vector: %f, %f\n", velocity->getX(), velocity->getY());
		state = IDLE;

		if (velocity->getY() > -0.1f) {
			state = DOWN; 
		}else if (velocity->getY() < 0.1f) {
				state = UP;
			}
		
		if (velocity->getX() > 0.8f) {
			state = RIGHT;
		}else if (velocity->getX() < -0.8f) {
				state = LEFT;
			}
		//will work but wobbles
		//velocity->scale(speed);

		//Arrive Pattern
		distance /= timeToTarget;

		if (velocity->length() > speed) {
			velocity->normalise();
			velocity->scale(speed);
		}
	}

}

void NPC::setGame(Game* game) {
	this->game = game;
}

void NPC::update(float dt) {
	ai();
	Sprite::update(dt);
}

/*void NPC::update(float timeDeltaInSeconds) {

	//Calculate distance travelle since the last update
	Vector2f movement(velocity);
	movement.scale(timeDeltaInSeconds);

	//Update player position
	position->add(&movement);

	//Move sprite to nearest pixel location
	targetRectangle.x = round(position->getX());
	targetRectangle.y = round(position->getY());

	//Get the current animation
	Animation* current = animations[getCurrentAnimationState()];

	//Let animation update itself
	current->update(timeDeltaInSeconds);
}*/